import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
  * Created by remya on 1/10/2016.
  *

  *
  */
object LogFile {
  //Why does it lose the log variable once run?
  val lines = scala.io.Source.fromFile("TestFile").getLines()
  val log = lines map { line => makeLog(line.split(",")) }


  def makeLog(line: Array[String]): LogClass = {
    LogClass(parseDate(line(0)), line(1), line(2))
  }
  def parseDate(date: String): LocalDateTime = {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    LocalDateTime.parse(date, formatter)
  }

  def ErrorDates(a: String): Unit = {
    //val lines = scala.io.Source.fromFile("TestFile").getLines()
    //val log = lines map { line => makeLog(line.split(",")) }
    val timestamp = log.toSeq.groupBy(_.errorCode) map { case (code,timeSeq) => code -> (timeSeq map { _.date})}
    val result = timestamp(a)
    result.foreach(println)
  }
  def ErrorFrequency(a: String): Unit = {
    //val lines = scala.io.Source.fromFile("TestFile").getLines()
    //val log = lines map { line => makeLog(line.split(",")) }
    val freq = log.toSeq.count( _.errorCode == a)
    println(freq)
  }
}

